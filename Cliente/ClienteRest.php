<?php  

// url del rest donde le pasamos parametros por POST.
$url = "http://localhost/EsteAnio/DWES/laravel/laravel_segundaEv_ALVARO/proyecto_laravel/public/rest/crear";

// array de los datos a pasar.
$datos = [
    'nombre' => 'Rex',    
    'imagen' => "imagen.jpg",
    'artista' => '1'
];
    
//url contra la que atacamos
$ch = curl_init($url);

//a true, obtendremos una respuesta de la url, en otro caso, 
//true si es correcto, false si no lo es.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//establecemos el verbo http que queremos utilizar para la petición.
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

//enviamos el array datos.
curl_setopt($ch, CURLOPT_POSTFIELDS, $datos);

//obtenemos la respuesta.
$response = curl_exec($ch);

// Se cierra el recurso CURL y se liberan los recursos del sistema.
curl_close($ch);

// decodifcar la respuesta json.
$respuesta_decodificada = json_decode($response);

// mostrarla.
print_r($respuesta_decodificada);
?>