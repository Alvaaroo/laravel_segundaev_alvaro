<?php 

// Url del wsdl
$url = 'http://localhost/EsteAnio/DWES/laravel/laravel_segundaEv_ALVARO/proyecto_laravel/public/api/wsdl';

// Crear cliente Soap.
$cliente = new SoapClient($url);

// Parametro.
$id = 1;

// muestra de el numero de obras de un autor.
echo $cliente->getNumeroObrasArtista($id);

echo "<br><br>";

// Muestra del nombre de las obras de un artista.
$obras = $cliente->getObrasArtista($id);

echo "Las obras del artista $id son: ";

foreach ($obras as $obra) {
	
	echo "<li>" . $obra->nombre . "</li>";
}



?>