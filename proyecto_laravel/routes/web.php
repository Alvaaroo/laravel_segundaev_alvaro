<?php

// use Illuminate\Support\Facades\Route;

use App\Artista;
use App\Obra;	


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Ruta del inicio.
Route::get('/', 'inicioController@getInicio');

// Ruta para mostrar todos los artistas.
Route::get('artistas', 'ArtistaController@getTodos');

// Ruta para ver la informacion especifica de un artista pasando como parametro su slug.
Route::get('artistas/ver/{slug}', 'ArtistaController@getArtista');

// Ruta para mostrar el formulario de la creacion de una nueva obra.
Route::get('obras/crear', 'ObrasController@getCrear');

//Ruta para mandar los datos del formulario y crear la obra.
Route::post('obras/crear', 'ObrasController@postCrear');

// Ruta donde se muestra la correcta creacion del soap Server.
Route::any('api', 'SoapServerController@getServer');

// Ruta que muestra el xml del wsdl
Route::any('api/wsdl', 'SoapServerController@getWSDL');

// Ruta donde se envia el parametro del formulario de creacion para buscar los autores que contengan esa cadena.
Route::post("busquedaAjax", 'ArtistaController@Buscar');

// Rutas de las distintas funciones del servicio Rest.
Route::get('rest/artistas', 'RestWebServiceController@getTodos');
Route::get('rest/artistas/{id}', 'RestWebServiceController@getVerArtista');
Route::post('rest/crear', 'RestWebServiceController@postCrear');
Route::get('rest/obras/{id}', 'RestWebServiceController@obrasAutor');


Route::get("prueba", function(){

	$obra = new Obra();

	echo $obra;

	if ($Request->has("nombre") && $Request->has("artista") && $Request->has('imagen')) {	

		$obra->nombre = $Request->nombre;
		$obra->imagen = $Request->imagen->store('', 'artistas');	
		$obra->artista_id = $Request->artista;

		$artistID = Artista::where("nombre", $obra->artista_id)->first();

		$obra->artista_id = $artistID->id;

	}else{

		return redirect('obras.crear')->withInput();
	}
	
	$obra->save();
					   // Con el with se guardan variables en session
	return redirect('artistas')->with('mensaje', "obra: $obra->nombre guardada correctamente");
});