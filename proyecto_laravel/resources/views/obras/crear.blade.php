@extends('layouts.master')
@section('titulo')
obras (crear)
@endsection
@section('contenido')
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">
				Añadir nueva obra
			</div>
			<div class="card-body" style="padding:30px">
				{{-- Action del formulario con método POST --}}
				<form action="{{action('ObrasController@postCrear')}}" method="POST" enctype="multipart/form-data">	
					{{-- Cifrado del formulario --}}
					{{ csrf_field() }}
				<div class="form-group">
						{{-- Iinput del nombre de la obra --}}
						<label for="nombre">Nombre de la obra</label>
						<input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre')}}">
					</div>
					{{-- Select para el nombre del autor que despues hemos transformado en un input de busqueda por nombre del artista --}}	
					<div class="form-group">
						<label for="nombre">Artista</label>						
						{{-- <select type="text" name="artista" id="artista" class="form-control">
							@foreach($artistas as $artista)

								<option>{{$artista->nombre}}</option>
							@endforeach
						</select> --}}

						<input id="artista" name="artista" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">						
					</div>						
					<div class="form-group">
						{{-- TODO: Completa el input para la imagen --}}
						<label for="nombre">Imagen</label>
						<input type="file" class="form-control-file" id="imagen" name="imagen" value="{{old('imagen')}}">
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
							Añadir obra
						</button>
					</div>					
				</form>
				<script>
			      $(document).ready(function() {
			        $('#artista').autocomplete({

			          source: 

			          function (query, result) 
			          {
			            $.ajax({

				            type: "POST",

				            url: "{{url('busquedaAjax')}}",

				            dataType: "json",

				            data: {"_token": "{{ csrf_token() }}", "busqueda": query['term']},

				            success : function(data)
				            {
				             result(data);
				            }
			           	});
			          },

			          position: {

			                  my: "left+0 top+8", //Para mover la caja 8px abajo

			                }
			              });
			      });

			    </script>
			</div>
		</div>
	</div>
</div>
@endsection