@extends('layouts.master')
@section('titulo')
  pinacoteca (ver)
@endsection
@section('contenido')
  <div class="container">    
    <div class="row">   
    <div class="col-xs-12 col-sm-1 col-md-6">
      <h3 style="min-height:45px;margin:5px 0 10px 0; color:#E74C3C;">{{$artistas->nombre}}</h3>
      <h4 style="min-height:45px;margin:5px 0 0px 0;">Pais: {{$artistas->pais}}</h4>  
      <h4 style="min-height:45px;margin:5px 0 0px 0;">Obras</h4>  
    </div>
    <div class="row" style="margin-left: 50px;">
      @foreach($artistas->obras as $obra)
        <div class="col-xs-12 col-sm-1 col-md-3">
        <h5 style="min-height:45px;margin:5px 0 10px 0; color:#7FB3D5;">{{$obra->nombre}}</h5>
        <img src="{{asset('assets/imagenes/')}}/{{$obra->imagen}}" alt="{{$obra->nombre}}" class="img-fluid w-100">
      </div>
      @endforeach
    </div>   
  </div>
  </div>
@endsection