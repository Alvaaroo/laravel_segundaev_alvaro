@extends('layouts.master')
@section('titulo')
pinacoteca (index)
@endsection
@section('contenido')
<div class="container">
	<table class="table" style="margin-top:90px;">
    <thead>
      <tr class="table-primary">
        <th>Nombre</th>
        <th>País</th>
        <th>Obras</th>
      </tr>
    </thead>
    <tbody> 
      @foreach($Artistas as $clave => $Artista)
      	<tr>
      		<td>
      			<a href="{{ url('/artistas/ver/' . $Artista->slug ) }}">
					<b>{{$Artista->nombre}}</b>					
				</a>
			</td>
      		<td>{{$Artista->pais}}</td>
      		<td>{{count($Artista->obras)}}</td>
      	</tr>
      @endforeach
    </tbody>
  </table>	
  @if (session('mensaje'))
    <div class="alert alert-success">
        {{ session('mensaje') }}
    </div>
@endif
</div>
@endsection