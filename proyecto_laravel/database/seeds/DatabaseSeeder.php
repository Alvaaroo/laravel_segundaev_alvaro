<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Artista;
use App\Obra;

class DatabaseSeeder extends Seeder
{

	private $arrayArtistas = array(
		array(
			'nombre' => 'Pablo Picasso',
			'pais' => 'España',
			'fechaNacimiento' => '1881-10-25',
			'obras' => array(
				array(
					'nombre' => 'Guernica',
					'imagen' => 'guernica.jpg'
				),
				array(
					'nombre' => 'Las señoritas de Avignon',
					'imagen' => 'senoritas-avignon.jpg'
				),
				array(
					'nombre' => 'La mujer que llora',
					'imagen' => 'mujer-llora.jpg'
				),
			),
		),

		array(
			'nombre' => 'Vincent van Gogh',
			'pais' => 'Países Bajos',
			'fechaNacimiento' => '1853-03-30',
			'obras' => array(
				array(
					'nombre' => 'Retrato del doctor Gachet',
					'imagen' => 'retrato_doctor_gachet.jpg'
				),
				array(
					'nombre' => 'La noche estrellada',
					'imagen' => 'noche-estrellada.jpg'
				),
			)
		),

		array(
			'nombre' => 'Salvador Dalí',
			'pais' => 'España',
			'fechaNacimiento' => '1904-05-11',
			'obras' => array(
				array(
					'nombre' => 'La persistencia de la memoria',
					'imagen' => 'persistencia_memoria.png'
				),
			),
		),

		array(
			'nombre' => 'Diego Velázquez',
			'pais' => 'España',
			'fechaNacimiento' => '1599-06-06',
			'obras' => array(
				array(
					'nombre' => 'Las meninas',
					'imagen' => 'meninas.jpg'
				),
				array(
					'nombre' => 'Vieja friendo huevos',
					'imagen' => 'vieja-friendo-huevos.jpg'
				),
			),
		),


		array(
			'nombre' => 'Francisco de Goya',
			'pais' => 'España',
			'fechaNacimiento' => '1746-03-30',
			'obras' => array(
				array(
					'nombre' => 'El 3 de mayo en Madrid',
					'imagen' => '3-mayo.jpg'
				),
				array(
					'nombre' => 'El coloso',
					'imagen' => 'coloso.jpg'
				),
			),
		),
		
	);

	//   Array ^


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        self::seedArtistas();
 		$this->command->info('Tabla Artistas y obras inicializada con datos');
    }

    public function seedArtistas(){

    	DB::table('Obras')->delete();
    	DB::table('Artistas')->delete();

    	foreach ($this->arrayArtistas as $Artista) {

    		$a = new Artista();
			$a->nombre = $Artista['nombre'];
			$a->slug = Str::slug($Artista['nombre']);
			$a->pais = $Artista['pais'];
			$a->fechaNacimiento = $Artista['fechaNacimiento'];			
    		$a->save();

    		$id = $a->id;

    		foreach ($Artista['obras'] as $Obras) {
    				
				$o = new Obra();		
				$o->nombre = $Obras['nombre'];
				$o->imagen = $Obras['imagen'];
				$o->artista_id = $id;
				$o->save();
    		}
    		   		
    	}
    }
}
