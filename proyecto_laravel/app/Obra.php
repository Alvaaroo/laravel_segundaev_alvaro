<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
* @property string $nombre
* @property string $imagen
*/
class Obra extends Model
{
    // Variables del modelo de Obra.
    protected $table = 'Obras';
	protected $primaryKey = 'id_obra';
	public $timestamps = false;
}
