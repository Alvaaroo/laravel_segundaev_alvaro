<?php

namespace App\Http\Controllers;

// Nota: Siempre hacen falta, no olvidar.
use Illuminate\Http\Request;

class inicioController extends Controller
{
    // Simple redirección de la pantalla de inicio a la vista de ver todos los autores.
    public function getInicio()
    {
    	return redirect()->action('ArtistaController@getTodos');
    	
    }
}
