<?php

namespace App\Http\Controllers;

// Nota: Siempre hacen falta, no olvidar.
use Illuminate\Http\Request;
use App\Artista;
use App\Obra;

class ObrasController extends Controller
{
    // Funcion para mandar a la vista del formulario de crear el nombre de todos los artistas (INCISO: esto es por que se tenian que enviar para mostrar los nombre de los artistas en un select, pero despues ya no hace falta).
    public function getCrear()
	{
		$Artistas = Artista::all();
		return view('obras.crear', array("artistas" => $Artistas));
	}	

	// Funcion donde se crea el objeto obra, se le pasan los parametros del formulario y si sale mal se redirige al formulario y si la crea correctamente se redirige a la pagina principal. enviando un mensaje.
	public function postCrear(Request $Request)
	{
		$obra = new Obra();

		if ($Request->has("nombre") && $Request->has("artista") && $Request->has('imagen')) {	

			$obra->nombre = $Request->nombre;
			$obra->imagen = $Request->imagen->store('', 'artistas');	
			$obra->artista_id = $Request->artista;

			$artistID = Artista::where("nombre", $obra->artista_id)->first();

			$obra->artista_id = $artistID->id;

		}else{

			return redirect('obras/crear')->withInput();
		}
		
		$obra->save();
						   // Con el with se guardan variables en session
		return redirect('artistas')->with('mensaje', "obra: $obra->nombre guardada correctamente");
	}
}