<?php

namespace App\Http\Controllers;

// Nota: Siempre hacen falta, no olvidar.
use Illuminate\Http\Request;
use App\Artista;

class ArtistaController extends Controller
{
    // Devuelve la vista de artistas pasandole un array con todas estas.
    public function getTodos()
    {
  		$Artistas = Artista::all();

		return view('artistas.index' , array("Artistas" => $Artistas));
    }

    // devuelve la vista de la información de un solo artista.
    public function getArtista($slug)
    {
    	$artistas = Artista::where("slug", $slug)->first();

    	return view('artistas.ver', array('artistas' => $artistas));
    }

    // Funcion para la utilización de AJAX en la busqueda en el input del nombre de un autor para su posterior creacion en el formulario de crear.
    public function Buscar(Request $request)
	{
		$busqueda = $request->busqueda;
		$artistas = Artista::select("nombre")->where('nombre', 'like', "%".$busqueda."%")->pluck('nombre');

		return response()->json($artistas);
	}

}
