<?php

namespace App\Http\Controllers;

// Nota: Siempre hacen falta, no olvidar.
use Illuminate\Http\Request;
use App\Artista;
use App\Obra;
use SoapServer;
use App\lib\WSDLDocument;

class SoapServerController extends Controller
{
    // Creacion de todas las variables necesarias para crear un servidor Soap.
    private $clase = "\\App\\Http\\Controllers\\ArtistasWebService";
    private $uri = "http://localhost/EsteAnio/DWES/laravel/laravel_segundaEv_ALVARO/proyecto_laravel/public/api";
    private $servidor = "http://localhost/EsteAnio/DWES/laravel/laravel_segundaEv_ALVARO/proyecto_laravel/public/api";
    private $UrlWSDL = "http://localhost/EsteAnio/DWES/laravel/laravel_segundaEv_ALVARO/proyecto_laravel/public/api/wsdl";

    // Funcioón para crear el servidor.
    public function getServer()
    {
        $server = new SoapServer($this->UrlWSDL);
        $server->setClass($this->clase);
        $server->handle();
        exit();
    }

    // Función para crear y generar el WSDL.
    public function getWSDL()
    {
        $wsdl = new WSDLDocument($this->clase, $this->servidor, $this->uri);
        $wsdl->formatOutput = true;
        header('Content-Type: text/xml');
        echo $wsdl->saveXML();
        exit();
    }
}

// clase donde implementaremos las funciones del soap que despues usaremos.
class ArtistasWebService
{
    /**
     * Funcion que devuelve un el numero de obras de un artista
     * @param int $id 
     * @return int
     */    
    public function getNumeroObrasArtista($id)
    {
        $artista = Artista::find($id);   

        return count($artista->obras);     
    }

    /**
     * Funcion que nos devuelve un array de todas las obras
     * @param int $id 
     * @return App\Obra[]
     */
    
    public function getObrasArtista($id)
    {
        $artista = Artista::find($id);   

        return $artista->obras;   
    }     
}