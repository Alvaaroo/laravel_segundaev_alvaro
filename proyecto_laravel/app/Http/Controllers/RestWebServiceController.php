<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Nota: Siempre hacen falta, no olvidar.
use App\Artista;
use App\Obra;

class RestWebServiceController extends Controller
{
    // Tdoas las funciones del rest de artistas.

    public function getTodos()
    {
        $Artistas = Artista::all();        
        return response()->json($Artistas);
    }

    public function getVerArtista($id)
    {       
        $Artista = Artista::where('id', $id)->first();        
        return response()->json($Artista);
    }

    // Tdoas las funciones del rest de obras.

    public function postCrear(Request $Request)
	{
		$obra = new Obra();

		if ($Request->has("nombre") && $Request->has("artista") && $Request->has('imagen'))
		{
			// Nota: siempre a de haber relacion con los nombres de la BD.
			$obra->nombre = $Request->nombre;
			$obra->imagen = $Request->imagen;	
			$obra->artista_id = $Request->artista;

		}else{

			return response()->json(['mensaje' => "Error al insertar nueva obra"]);
		}
		
		// Nota: No olvidar que hay que guardar la obra o si no no sirve de nada lo anterior.
		$obra->save();
						  
		return response()->json(['mensaje' => "Obra insertada correctamente"]);
	}

	// He añadido esta nueva funcion que te devuelve el array de las obras de un autor.

	public function obrasAutor($id)
	{
		$Artista = Artista::where('id', $id)->first();

		return response()->json($Artista->obras);
	}

}
