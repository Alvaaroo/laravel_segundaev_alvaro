<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
* @property string $nombre
* @property string $pais
* @property string $fechaNacimiento
*/
class Artista extends Model
{
    // Variables del modelo de Artista.
    protected $table = 'Artistas';
	protected $primaryKey = 'id';
	public $timestamps = false;

	// Función para relacionar el artista con sus obras.
	public function obras()
	{
		return $this->hasMany("App\Obra");
	}
}
